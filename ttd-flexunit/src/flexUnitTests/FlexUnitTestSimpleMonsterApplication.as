package flexUnitTests
{
	import net.pirsquare.test.mind.MyMonster;
	import net.pirsquare.test.mind.MyMonsterEvent;

	import org.flexunit.asserts.assertFalse;
	import org.flexunit.asserts.assertNull;

	public class FlexUnitTestSimpleMonsterApplication
	{
		private var _monster:MyMonster;

		[Before]
		public function setUp():void
		{
			// setup monster
			_monster = new MyMonster(100);
		}

		[After]
		public function tearDown():void
		{
			_monster.dead();
			_monster.finish();
		}

		[Test]
		public function run():void
		{
			_monster.addEventListener(MyMonsterEvent.ATK, attack);
			_monster.addEventListener(MyMonsterEvent.DEAD, dead);

			_monster.attack('me');

			// when attack			
			function attack(e:MyMonsterEvent):void
			{
				if (!e.object)
				{
					assertNull('assertNull[ไม่มีObjectอะไรส่งมา]');
					return;
				}

				if (e.object.atk > _monster.atk)
					assertFalse('assertFalse[ต่า ATK ไม่ถูกต้อง]');

				if (!e.object.target)
					assertNull('assertNull[ไม่มี TARGET]');
			}

			// when die
			function dead(e:MyMonsterEvent):void
			{
				if (!e.object)
					assertNull('assertNull[ไม่มีObjectอะไรส่งมา]');

				if (!e.object.exp)
					assertNull('assertNull[ไม่มี EXP]');
			}
		}

		[BeforeClass]
		public static function setUpBeforeClass():void
		{
		}

		[AfterClass]
		public static function tearDownAfterClass():void
		{
		}


	}
}
