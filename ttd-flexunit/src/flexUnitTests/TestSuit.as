package flexUnitTests
{
	import FlexUnitCompilerApplication;

	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class TestSuit
	{
		public var test1:FlexUnitCompilerApplication;
		public var test2:FlexUnitTestSimpleMonsterApplication;
	}
}
