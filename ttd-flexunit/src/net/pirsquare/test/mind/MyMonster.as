package net.pirsquare.test.mind
{
	import flash.events.EventDispatcher;

	public class MyMonster extends EventDispatcher
	{
		// properties ========================================================================
		private var _atk:int;

		public function get atk():int
		{
			return _atk;
		}

		// constructor ========================================================================
		public function MyMonster(atk:int)
		{
			_atk = atk;
		}

		// action =============================================================================
		public function attack(targetName:String):void
		{
			dispatchEvent(new MyMonsterEvent(MyMonsterEvent.ATK, true, {'atk': Math.floor(Math.random() * _atk), 'target': targetName}));
		}

		public function dead():void
		{
			dispatchEvent(new MyMonsterEvent(MyMonsterEvent.DEAD, false, {'exp': Math.floor(Math.random() * 10)}));
		}

		// method =============================================================================

		public function finish():void
		{
		}

	}
}
