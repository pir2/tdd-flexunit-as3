package net.pirsquare.test.mind
{
	import flash.events.Event;

	public class MyMonsterEvent extends Event
	{

		/**
		 * when atk event name
		 */
		public static const ATK:String = "atk";

		/**
		 * when dead event name
		 */
		public static const DEAD:String = "dead";


		/**
		 * my monster dispacher object
		 */
		private var _object:Object;

		public function get object():Object
		{
			return _object;
		}

		/**
		 * @param type : is event name look at MyMonsterEvent.ATK or MyMonsterEvent.DEAD
		 * @param bubbles : global dispach
		 * @param object : my object idpsach
		 * @param cancelable : can cancle
		 */
		public function MyMonsterEvent(type:String, bubbles:Boolean = false, object:Object = null, cancelable:Boolean = false)
		{
			_object = object;

			super(type, bubbles, cancelable);
		}
	}
}
