package net.pirsquare.test.mind
{

	public class SimpleUnitTestFunction
	{
		/**
		 *
		 * @param a
		 * @param b
		 * @return
		 *
		 */
		public static function compareInt(a:int, b:int):Boolean
		{
			return a == b;
		}

		/**
		 *
		 * @param a
		 * @param b
		 * @return
		 *
		 */
		public static function addInt(a:int, b:int):int
		{
			return a + b;
		}
	}
}
